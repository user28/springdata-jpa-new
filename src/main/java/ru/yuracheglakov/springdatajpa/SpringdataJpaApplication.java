package ru.yuracheglakov.springdatajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.yuracheglakov.springdatajpa.entity.UsersEntity;
import ru.yuracheglakov.springdatajpa.repository.UsersRepository;

import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration
public class SpringdataJpaApplication implements CommandLineRunner {

    @Autowired
    UsersRepository usersRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringdataJpaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<UsersEntity> users = usersRepository.findAll();

        for(UsersEntity user: users) {
            System.out.println(String.format("%s %s", user.getFirstName(), user.getLastName()));

        }
    }
}
