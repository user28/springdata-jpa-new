package ru.yuracheglakov.springdatajpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.yuracheglakov.springdatajpa.entity.UsersEntity;

import java.util.List;

@Repository("UsersRepository")
public interface UsersRepository extends JpaRepository<UsersEntity, Integer> {
    List<UsersEntity> findByFirstName(String firstName);

    List<UsersEntity> findByFirstNameAndLastName(String firstName, String lastName);
}
